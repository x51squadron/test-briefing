### README

This is an example presentation using gitpitch. The PITCHME.md is rendered into 
slides which can be viewed at `gitpitch.com/<username>/<project-name>?grs=gitlab`

This one is viewable here:
https://gitpitch.com/x51squadron/test-briefing?grs=gitlab

To make a new briefing, use the template made from this example here:
https://gitlab.com/x51squadron/gitpitch-briefing-template

Either branch it, or fork it for each new briefing. They will automatically
embed in the news/events posts on the X51 site.
